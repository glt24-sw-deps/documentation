# Shepherding Software Dependencies

This document provides additional details and materials for the talk
"[Shepherding Software Dependencies](https://pretalx.linuxtage.at/glt24/talk/DXJLMZ/)"
given at the [Grazer Linuxtage 2024](https://www.linuxtage.at).

## Slides an Recording

* [Slides - Shepherding_Software_Dependencies_v1-0-0.pdf](./slides/Shepherding_Software_Dependencies_v1-0-0.pdf)
* [Recording on media.ccc.de](https://media.ccc.de/v/glt24-464-shepherding-software-dependencies)

## Example Projects

* [NPM Demo](https://gitlab.com/glt24-sw-deps/npm-demo)
* [Spring Boot Demo](https://gitlab.com/glt24-sw-deps/spring-boot-demo)
* [Renovate Runner](https://gitlab.com/glt24-sw-deps/renovate-runner)
* [Renovate Demo](https://gitlab.com/glt24-sw-deps/renovate-demo)

## Example SBOMs

* [NPM Demo SBOM](./sboms/npm-demo.sbom.json)
* [Spring Boot Demo SBOM](./sboms/spring-boot-demo.sbom.json)
